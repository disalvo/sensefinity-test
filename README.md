
# BOOTSTRAP-PY #

  

Boostrap development project for container based Sensefinity Cloud services built in Python.

### ASSESSMENT EXERCISE ###

**After reading** the goal of the exercise, **check below explanations** on how to deal with the development environment.

Please, any questions get in touch with us by email.

Current working project gets Lisbon weather directly from an **OpenWeather** URL and write it on the log screen, as you can see running docker-compose.
The goal is, with a **REST solution**, to **receive a code** from any **city**, **check** if the requested code **is already in Redis** key value system with weather information, if exists use it from "cache", **if not get from OpenWeather once again**.

You can **see how to get cities ID's** for your tests as explained here [Part 3: Get your OpenWeatherMap City ID](https://www.dmopress.com/openweathermap-howto/)

 1. **Create a JSON RESTfull service**, that **accept a GET request with a city ID** (you can use any PORT you want as example here we suggest 5656), **e.g.** São Paulo Brazil weather could be requested with URL [GET Method] http://localhost:5656/weather/city/3448433
 2. After receiving the request **check if it already exists in a cache** solution, for this exercise should be **Redis** (note you need to set Redis in the docker-compose configuration file so it is running up together with the current weather REST API service)
 3. **If it exists in Redis, use the one already there**, **if** it does **not** exist **go** directly **to OpenWeather**, get the info, return it for the user request after **saving it in Redis** for the next requests.
 4. **Important to note**, **cached data in Redis should not persist there for more than 30 minutes**. So every time you record a city **data in Redis** for future usage, it **should only be there for the next 30 minutes**.

> This assessment will evaluate how you structure code, types,
> variables, nomenclatures, move data across functions and how far you go to have a reliable code in place.


### How do I get set up? ###

    git clone git@gitlab.com:sensefinity-world/bootstrap-py.git

rename the folder to your project name

    mv bootstrap-py the-new-name

move in to the folder

    cd the-new-name

checkout assessment branch (should have been sent by email)

    git checkout assessment-level-date
    
and remove the .git folder

    rm -fr .git

  

Now the project have no connection with the original repository. Further on to have it in a new repository create a new project at an online repositories manager e.g. Gitlab, Github,... with your own account, and push your solution there, after finishing send us the URL of where you pushed your work to by email.

  
### Main files to checkout nomenclatures ###

* Dockerfile

    SERVICE_NAME=bootstrap-py
    SERVICE_DESC='Boostrap project for Python based services' \
    SERVICE_TAGS='local,local-test,sens8,sensefinity-bootstrap-py,bootstrap-py' \

* docker-compose.yml

    services:
    bootstrap-py:
    image: local/bootstrap-py

### Available suggested connectors ###

* Redis
https://pypi.org/project/redis/
  

### Executing

  
To run the solution, at project root, execute

    docker-compose up --build

  
 
### Developing

For development after executing docker-compose, if on a wait status service, e.g. REST, all files while saved will do an automatic re-run of the code thanks to the file *app/init.dev.sh*

> So for alive services, no need to turn down docker-compose and turn up again to see changes of the code being executed.

  
  

### Testing

  
**Tests are named test_*** and are inside *app/tests/* folder

To run the solution tests, at project root, execute

    docker-compose -f tests/docker-compose.yml up --build

Project is using unittest from Python

Check for assert methods here https://docs.python.org/3/library/unittest.html#assert-methods

